var express = require('express')
var session = require('express-session')
var app = express()
var fs = require('fs')
var path = require("path");

var http = require('http')
var request = require('request')
var config = require('./config')
var passport = require('passport')
var TwitterStrategy = require('passport-twitter').Strategy

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'secret'
}))

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}

app.use(allowCrossDomain);

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

var user = {}

passport.use(new TwitterStrategy({
        consumerKey: config.consumerKey,
        consumerSecret: config.consumerSecret,
        callbackURL: config.callbackURL
    },
    function(token, tokenSecret, profile, done) {

        var options = {
            uri: 'http://127.0.0.1:8000/api/user/' + profile.id,
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };

        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
                user = JSON.parse(body)[0]
                if(user == undefined || user == null){
                    console.log("Création du nouvelle utilisateur")
                    user = {
                        "id" : profile.id,
                        "url" : profile._json.url,
                        "name": profile.username,
                        "profile_image_url": profile.photo,
                        "acces_token_key" : token,
                        "acces_token_secret" : tokenSecret
                    }
                    var options = {
    					uri: 'http://127.0.0.1:8000/api/user',
    					method: 'POST',
    					json: user
    				};

                    request(options, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log("Bon déroulement de l'ajout de l'utilisateur")
                        }
                    });
                }
            }
            done(null, user);
        });
    }
));

app.use(express.static(process.cwd() + '/public'));

app.get('/', function (req, res) {
   res.sendFile(path.join(__dirname+'/index.html'));
})

app.get('/profil/json', function(req, res){
    return res.json(user)
});

app.get('/auth/twitter',
  passport.authenticate('twitter'));

app.get('/auth/twitter/callback',
  passport.authenticate('twitter', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
 });

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

var server = app.listen(3000, function(){
    var port = server.address().port;
    console.log('Connexion sur l\'adresse 127.0.0.1:%s', port);
});

module.exports = app;
