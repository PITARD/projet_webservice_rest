var http = require('http')
var config = require('config')
var passport = require('passport')
var TwitterStrategy = require('passport-twitter').Strategy

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'secret'
}))

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(new TwitterStrategy({
        consumerKey: config.consumerKey,
        consumerSecret: config.consumerSecret,
        callbackURL: config.callbackURL
    },
    function(token, tokenSecret, profile, done) {

	 	var options = {
			host: 'www.127.0.0.1:8000',
		  	path: '/api/user' + profile.id
		}

		callback = function(response) {
		  var str = '';

		  //another chunk of data has been recieved, so append it to `str`
		  response.on('data', function (chunk) {
		    str += chunk;
		  });

		  //the whole response has been recieved, so we just print it out here
		  response.on('end', function () {
		    console.log(str);
		  });
		}

		http.request(options, callback).end();

        /*User.findOne({ 'id': profile.id }, function (err, user) {
            if(err) return done(err);

            if(user) {
                console.log('User: ' + profile._json.name + ' logged in!');
                done(null, user);
            } else if (profile._json.memberOf.indexOf('<CN of specific group allowed to log in>') != -1) {
                var newUser = new User();

                newUser._id = profile.id;
                newUser.username = profile.username;

                newUser.save(function(err) {
                    if(err) throw err;
                    console.log('New User: ' + newUser.username + ' created and logged in!');
                    done(null, newUser);
                });
            }
        });*/
    }
));

//The url we want is: 'www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'




