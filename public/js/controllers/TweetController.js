'use strict';
myApp.controller('TweetController', ['$scope', '$http', function($scope, $http){
	$scope.currentNavItem = 'tweets'
	$scope.user = {}
	$scope.tweets = []

	$scope.lstTweets = [];
	$scope.lstMyTweets = [];

    $http.get( "/profil/json").success(function( data ) {
        $scope.user = data; //from your sample;
        $http.get("http://127.0.0.1:8000/api/tweets/" + data.id)
        	.success(function(data){
        		var allData = []
        		console.log(data.length)
			for (var i = 0; i < data.length; i++) {
				allData.push(data[i])
			}
			var tabreduce = allData.reduce( (valeurPrécédente, valeurCourante) => valeurPrécédente.concat(valeurCourante))
	            $scope.tweets = tabreduce //from your sample;
	            console.log(allData.length)
        });
        $scope.user = data;
    	$scope.getAllTweets();
    });

    $scope.getAllTweets = function(){
		if($scope.user){
			$http.get("http://127.0.0.1:8000/api/tweets/" + $scope.user.id)
	        	.success(function(data){
	        		var allData = []
				for (var i = 0; i < data.length; i++) {
					allData.push(data[i])
				}
		            $scope.lstTweets = allData
	        });
    	}else{
    		console.log('User undefined');
    	}
    }

    $scope.getMyTweets = function(){
    	if($scope.user){
			$http.get("http://127.0.0.1:8000/api/mytweets/" + $scope.user.id)
	        	.success(function(data){
		            $scope.lstMyTweets = data
	        });
    	}else{
    		console.log('User undefined');
    	}
    }

    $scope.saveTweet = function(tweetData){
    	
    	
		var tweet = {
		  	"user_id": $scope.user.id,
			"id" :  tweetData.id,
			"created_at" : tweetData.created_at,
			"urls" : '',
			"hashtags": tweetData.hashtags,
			"user_mentions": tweetData.user_mentions,
			"text" : tweetData.text,
			"retweeted" : tweetData.retweeted,
			"user" : tweetData.user
		}

		$http.post('http://127.0.0.1:8000/api/mytweets', tweet).
		    success(function(data, status, headers, config) {
		        console.log(data);
		    }).
		    error(function(data, status, headers, config) {
		        
		    });
	}

	$scope.deleteTweet = function(id){
		$http({
	        url: 'http://localhost:8000/api/mytweets/' + $scope.user.id,
	        method: 'DELETE',
	        data: {
	            tweet_id: id
	        },
	        headers: {
	            "Content-Type": "application/json;charset=utf-8"
	        }
	    }).then(function(res) {
	        $scope.getMyTweets()
	    }, function(error) {
	        console.log(error);
	    });
	}
        
}]);
